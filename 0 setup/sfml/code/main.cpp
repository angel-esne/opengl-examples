
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2014.02

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

using namespace sf;

namespace
{

    constexpr int window_width  = 1024;
    constexpr int window_height = 640;

    void prepare_opengl_for_rendering ()
    {
        glMatrixMode   (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho        (0, GLdouble(window_width), 0, GLdouble(window_height), +1, -1);
        glViewport     (0, 0, window_width, window_height);
        glMatrixMode   (GL_MODELVIEW);
    }

    void render_using_opengl ()
    {
        // Se dibuja un triángulo usando OpenGL clásico:

        glLoadIdentity ();
        glTranslatef   (GLfloat(window_width) / 2.f, GLfloat(window_height) / 2.f, 0.f);

        glBegin (GL_TRIANGLES);
        {
            glColor3f  (   1.f,    0.f, 0.f);
            glVertex3f (-200.f, -200.f, 0.f);
            glColor3f  (   0.f,    1.f, 0.f);
            glVertex3f (+200.f, -200.f, 0.f);
            glColor3f  (   0.f,    0.f, 1.f);
            glVertex3f (   0.f,  200.f, 0.f);
        }
        glEnd ();
    }

}

int main ()
{
    Window window
    (
        VideoMode(window_width, window_height),
        "OpenGL Setup using SFML",
        sf::Style::Default,
        ContextSettings()
    );

    window.setVerticalSyncEnabled (true);
    
    prepare_opengl_for_rendering ();

    bool running = true;

    do
    {
        Event event;

        while (window.pollEvent (event))
        {
            if (event.type == Event::Closed)
            {
                running = false;
            }
        }

        // Se borra el framebuffer y se dibuja algo:

        glClear (GL_COLOR_BUFFER_BIT);

        render_using_opengl ();

        // Se actualiza el contenido de la ventana:

        window.display ();
    }
    while (running);

    return (EXIT_SUCCESS);
}
