
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2014.02

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN

#include <windows.h>
#include <GL/GL.h>

namespace
{

    constexpr int window_width  = 1024;
    constexpr int window_height = 640;

    void prepare_opengl_for_rendering ()
    {
        glMatrixMode   (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho        (0, GLdouble(window_width), 0, GLdouble(window_height), +1, -1);
        glViewport     (0, 0, window_width, window_height);
        glMatrixMode   (GL_MODELVIEW);
    }

    void render_using_opengl ()
    {
        // Se dibuja un tri�ngulo usando OpenGL cl�sico:

        glLoadIdentity ();
        glTranslatef   (GLfloat(window_width) / 2.f, GLfloat(window_height) / 2.f, 0.f);

        glBegin (GL_TRIANGLES);
        {
            glColor3f  (   1.f,    0.f, 0.f);
            glVertex3f (-200.f, -200.f, 0.f);
            glColor3f  (   0.f,    1.f, 0.f);
            glVertex3f (+200.f, -200.f, 0.f);
            glColor3f  (   0.f,    0.f, 1.f);
            glVertex3f (   0.f,  200.f, 0.f);
        }
        glEnd ();
    }

}

LRESULT CALLBACK WndProc (HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
    static HGLRC opengl_context;
    static HDC   device_context;

    switch (message)
    {
        case WM_CREATE:
        {
            static const PIXELFORMATDESCRIPTOR pixel_format_descriptor =
            {
                sizeof(PIXELFORMATDESCRIPTOR),                                  // tama�o de la estructura
                1,                                                              // versi�n
                PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,     // requisitos
                PFD_TYPE_RGBA,                                                  // RGBA o indexado
                32,                                                             // bits por p�xel
                0, 0, 0, 0, 0, 0,                                               // bits de  color
                0,                                                              // no alpha
                0,                                                              // shift bit ignored
                0,                                                              // sin b�fer de acumulaci�n
                0, 0, 0, 0,                                                     // bits de acumulaci�n
                24,                                                             // bits del z-buffer
                0,                                                              // sin stencil buffer
                0,                                                              // sin b�fer auxiliar
                PFD_MAIN_PLANE,                                                 // capa principal
                0,                                                              // reservado
                0, 0, 0                                                         // m�scaras de capa
            };

            device_context = GetDC (window);

            int pixel_format_index = ChoosePixelFormat (device_context, &pixel_format_descriptor);

            SetPixelFormat (device_context, pixel_format_index, &pixel_format_descriptor);

            opengl_context = wglCreateContext (device_context);

            wglMakeCurrent (device_context, opengl_context);

            prepare_opengl_for_rendering ();

            break;
        }

        case WM_PAINT:
        {
            PAINTSTRUCT paint_data;

            BeginPaint (window, &paint_data);

            if (opengl_context)
            {
                glClear (GL_COLOR_BUFFER_BIT);

                render_using_opengl ();

                SwapBuffers (device_context);
            }

            EndPaint (window, &paint_data);

            break;
        }

        case WM_DESTROY:
        {
            if (opengl_context)
            {
                wglMakeCurrent   (device_context, NULL);
                wglDeleteContext (opengl_context);
            }

            ReleaseDC       (window, device_context);
            PostQuitMessage (0);

            break;
        }

        default:
        {
            return (DefWindowProc (window, message, wParam, lParam));
        }
    }

    return (0);
}

int CALLBACK WinMain (__in HINSTANCE application_instance, __in_opt HINSTANCE, __in_opt LPSTR, __in int)
{
    WNDCLASS window_class = { 0 };

    window_class.lpfnWndProc   = WndProc;
    window_class.hInstance     = application_instance;
    window_class.hbrBackground = (HBRUSH)(COLOR_BACKGROUND);
    window_class.lpszClassName = L"oglexample";
    window_class.style         = CS_OWNDC;

    if (!RegisterClass (&window_class))
    {
        return (-1);
    }

    CreateWindowW
    (
        window_class.lpszClassName,
        L"OpenGL Setup using the Windows SDK",
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        0,
        0,
        window_width,
        window_height,
        0,
        0,
        application_instance,
        0
    );

    MSG message;

    while (GetMessage (&message, NULL, 0, 0) > 0)
    {
        DispatchMessage (&message);
    }

    return (0);
}
