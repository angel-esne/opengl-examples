
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2014.02

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

using namespace sf;

namespace
{

    void reset_viewport (int width, int height)
    {
        glMatrixMode   (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho        (0, GLdouble(width), 0, GLdouble(height), +1, -1);
        glViewport     (0, 0, width, height);
    }

    void fill_circle (float center_x, float center_y, float radius)
    {
        glBegin (GL_TRIANGLE_FAN);
        {
            // Se añade el primer vértice en el centro:

            glVertex2f (center_x, center_y);

            // Se añaden vértices sobre el borde del círculo:

            for (float angle = 0.f; angle < 6.283185f; angle += 0.1f)
            {
                glVertex2f
                (
                    center_x + std::cosf (angle) * radius,
                    center_y + std::sinf (angle) * radius
                );
            }

            // Se añade un último vértice de cierre donde angle vale 0

            glVertex2f (center_x + radius, center_y);
        }
        glEnd ();
    }

}

int main ()
{
    int window_width  = 800;
    int window_height = 600;

    Window window(VideoMode(window_width, window_height), "OpenGL Examples: Circle 2D", Style::Default, ContextSettings(32));

    window.setVerticalSyncEnabled (true);

    glDisable (GL_CULL_FACE );
    glDisable (GL_DEPTH_TEST);

    reset_viewport (window_width, window_height);

    bool running = true;

    do
    {
        Event event;

        while (window.pollEvent (event))
        {
            switch (event.type)
            {
                case Event::Closed:
                {
                    running = false;
                    break;
                }

                case Event::Resized:
                {
                    Vector2u window_size = window.getSize ();

                    window_width  = window_size.x;
                    window_height = window_size.y;

                    reset_viewport (window_width, window_height);

                    break;
                }
            }
        }

        glClear   (GL_COLOR_BUFFER_BIT);
        glColor3f (0.f, 0.f, 1.f);

        fill_circle (float(window_width / 2), float(window_height / 2), 150.f);

        window.display ();
    }
    while (running);

    return (EXIT_SUCCESS);
}
