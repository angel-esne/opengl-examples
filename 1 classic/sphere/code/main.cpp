
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2014.03
// Pulsa las teclas + y - para aumentar y reducir las subdivisiones.

#include "View.hpp"
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

using namespace sf;
using namespace example;

int main ()
{
    Window window(VideoMode(800, 600), "OpenGL Examples: Sphere", Style::Default, ContextSettings(32));
    View   view  (800, 600);

    window.setVerticalSyncEnabled (true);

    bool running = true;

    do
    {
        Event event;

        while (window.pollEvent (event))
        {
            switch (event.type)
            {
                case Event::Closed:
                {
                    running = false;

                    break;
                }

                case Event::Resized:
                {
                    Vector2u window_size = window.getSize ();

                    view.resize (window_size.x, window_size.y);

                    break;
                }

                case Event::KeyPressed:
                {
                    view.on_key (event.key.code);

                    break;
                }
            }
        }

        glClear (GL_COLOR_BUFFER_BIT);

        view.update ();
        view.render ();

        window.display ();
    }
    while (running);

    return (EXIT_SUCCESS);
}
