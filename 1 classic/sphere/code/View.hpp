
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2014.03

#ifndef VIEW_HEADER
#define VIEW_HEADER

    #include <memory>
    #include "Sphere.hpp"

    namespace example
    {

        class View
        {
        private:

            std::auto_ptr< Sphere > sphere;

            float light_location[4];
            float angle;
            int   subdivisions;

        public:

            View(int width, int height);

            void create_sphere (int subdivisions);

            void update ();
            void render ();
            void resize (int width, int height);
            void on_key (int key_code);

        };

    }

#endif
