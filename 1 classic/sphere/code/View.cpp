
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2014.03

#include "View.hpp"
#include <SFML/OpenGL.hpp>
#include <SFML/Window/Keyboard.hpp>

namespace example
{

    void gluPerspective(GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar)
    {
        GLdouble height = tan(fovY / 360 * 3.1415926535897932384626433832795) * zNear;
        GLdouble width  = height * aspect;

        glFrustum (-width, +width, -height, +height, zNear, zFar);
    }

    View::View(int width, int height)
    {
        light_location[0] =   0.f;
        light_location[1] =  50.f;
        light_location[2] = 100.f;
        light_location[3] =   1.f;

        glEnable (GL_CULL_FACE);                // Descartar las back faces
        glEnable (GL_LIGHTING );                // Activar la iluminaci�n
        glEnable (GL_LIGHT0   );                // Activar la luz 0
        //glEnable (GL_COLOR_MATERIAL);         // Tomar las propiedades ambiental y difusa del material
                                                //   a partir del color activo
        glShadeModel  (GL_FLAT);                // No difuminar el color en el interior de los pol�gonos

        glPolygonMode (GL_FRONT, GL_LINE);      // Dibujar s�lo el borde de los pol�gonos

        // Activar el antialiasing de l�neas:

        glEnable      (GL_BLEND);
        glEnable      (GL_LINE_SMOOTH);
        glHint        (GL_LINE_SMOOTH_HINT, GL_NICEST);
        glBlendFunc   (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // Crear la esfera y preparar la ventana y la animaci�n:

        create_sphere (10);

        resize (width, height);

        angle = 0.f;
    }

    void View::create_sphere (int subdivisions)
    {
        if (subdivisions < 2)
        {
            subdivisions = 2;
        }

        sphere.reset (new Sphere(1.5f, subdivisions, subdivisions * 2));

        this->subdivisions = subdivisions;
    }

    void View::update ()
    {
        angle += 0.5f;
    }

    void View::render ()
    {
        // Resetear la matriz MODELVIEW:

        glLoadIdentity ();

        // Establecer la posici�n de la luz:

        glLightfv (GL_LIGHT0, GL_POSITION, light_location);

        // Se rota la esfera y se empuja hacia el fondo:

        glTranslatef (0.f, 0.f, -10.f);
        glRotatef    (angle, 1.f, 2.f, 1.f);

        // Se dibuja la esfera:

        sphere->render ();
    }

    void View::resize (int width, int height)
    {
        glMatrixMode   (GL_PROJECTION);
        glLoadIdentity ();
        gluPerspective (20, GLdouble(width) / height, 1, 15);
        glViewport     (0, 0, width, height);
        glClearColor   (0.f, 0.f, 0.f,  1.f);
        glMatrixMode   (GL_MODELVIEW);
    }

    void View::on_key (int key_code)
    {
        switch (key_code)
        {
            case sf::Keyboard::Add:
            case sf::Keyboard::Up:
            case sf::Keyboard::Right:
            {
                create_sphere (++subdivisions);
                break;
            }

            case sf::Keyboard::Subtract:
            case sf::Keyboard::Down:
            case sf::Keyboard::Left:
            {
                create_sphere (--subdivisions);
                break;
            }
        }
    }

}
