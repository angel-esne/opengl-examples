
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2014.03

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include "Circle.hpp"

using namespace sf;

namespace
{

    void reset_viewport (int width, int height)
    {
        glMatrixMode   (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho        (0, GLdouble(width), 0, GLdouble(height), +1, -1);
        glViewport     (0, 0, width, height);
        glMatrixMode   (GL_MODELVIEW);
    }

}

int main ()
{
    int window_width  = 800;
    int window_height = 600;

    Window window(VideoMode(window_width, window_height), "OpenGL Examples: Circle", Style::Default, ContextSettings(32));

    window.setVerticalSyncEnabled (true);

    glDisable (GL_CULL_FACE );
    glDisable (GL_DEPTH_TEST);

    reset_viewport (window_width, window_height);

    example::Circle circle(150.f, 0, 128, 255);

    bool running = true;

    do
    {
        Event event;

        while (window.pollEvent (event))
        {
            switch (event.type)
            {
                case Event::Closed:
                {
                    running = false;
                    break;
                }

                case Event::Resized:
                {
                    Vector2u window_size = window.getSize ();

                    window_width  = window_size.x;
                    window_height = window_size.y;

                    reset_viewport (window_width, window_height);

                    break;
                }
            }
        }

        glClear (GL_COLOR_BUFFER_BIT);

        glLoadIdentity ();
        glTranslatef   (float(window_width / 2), float(window_height / 2), 0.f);

        circle.render  ();

        window.display ();
    }
    while (running);

    return (EXIT_SUCCESS);
}
