
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2014.03

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

using namespace sf;

namespace
{

    void gluPerspective (GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar)
    {
        GLdouble height = tan(fovY / 360 * 3.1415926535897932384626433832795) * zNear;
        GLdouble width  = height * aspect;

        glFrustum (-width, +width, -height, +height, zNear, zFar);
    }

    void reset_viewport (int width, int height)
    {
        glMatrixMode   (GL_PROJECTION);
        glLoadIdentity ();
        gluPerspective (20, GLdouble(width) / height, 1, 15);
        glViewport     (0, 0, width, height);
        glMatrixMode   (GL_MODELVIEW);
    }

    void update_cube ()
    {
        static float angle = 0.f;

        angle += 0.25f;

        glLoadIdentity ();                          // Resetear la matriz MODELVIEW
        glTranslatef   (0.f, 0.f, -10.f);           // Trasladar hacia el fondo
        glRotatef      (28.f, 1.f, 0.f, 0.f);       // Rotar un poco alrededor del eje X
        glRotatef      (angle, 0.f, 1.f, 0.f);      // Rotar alrededor del eje Y
    }

    void render_cube ()
    {
        static const GLfloat vertices[][3] =
        {
            { -1.f, -1.f, +1.f },   // 0
            { +1.f, -1.f, +1.f },   // 1
            { +1.f, +1.f, +1.f },   // 2
            { -1.f, +1.f, +1.f },   // 3
            { -1.f, -1.f, -1.f },   // 4
            { +1.f, -1.f, -1.f },   // 5
            { +1.f, +1.f, -1.f },   // 6
            { -1.f, +1.f, -1.f },   // 7
        };

        static const GLubyte colors[][3] =
        {
            { 255,   0,   0 },      // 0
            {   0, 255,   0 },      // 1
            {   0,   0, 255 },      // 2
            {   0,   0, 255 },      // 3
            { 255, 255,   0 },      // 4
            { 255,   0, 255 },      // 5
            { 255,   0,   0 },      // 6
            { 255,   0,   0 },      // 7
        };

        static const GLint triangles[][3] =
        {
            { 0, 1, 2 },            // front
            { 0, 2, 3 },
            { 4, 0, 3 },            // left
            { 4, 3, 7 },
            { 5, 4, 7 },            // back
            { 5, 7, 6 },
            { 1, 5, 6 },            // right
            { 1, 6, 2 },
            { 3, 2, 6 },            // top
            { 3, 6, 7 },
            { 0, 4, 5 },            // bottom
            { 0, 5, 1 },
        };

        static const size_t number_of_triangles = sizeof(triangles) / sizeof(int) / 3;

        glBegin (GL_TRIANGLES);
        {
            for (size_t triangle_index = 0; triangle_index < number_of_triangles; triangle_index++)
            {
                const GLint   * const indices = triangles[triangle_index];
                const GLubyte * const color   = colors   [indices[0]];
                const GLfloat * const vertex0 = vertices [indices[0]];
                const GLfloat * const vertex1 = vertices [indices[1]];
                const GLfloat * const vertex2 = vertices [indices[2]];

                glColor3ubv (color  );
                glVertex3fv (vertex0);
                glVertex3fv (vertex1);
                glVertex3fv (vertex2);
            }
        }
        glEnd ();
    }

}

int main ()
{
    int window_width  = 800;
    int window_height = 600;

    Window window(VideoMode(window_width, window_height), "OpenGL Examples: Cube", Style::Default, ContextSettings(32));

    window.setVerticalSyncEnabled (true);

    glEnable (GL_CULL_FACE );

    reset_viewport (window_width, window_height);

    bool running = true;

    do
    {
        Event event;

        while (window.pollEvent (event))
        {
            switch (event.type)
            {
                case Event::Closed:
                {
                    running = false;
                    break;
                }

                case Event::Resized:
                {
                    Vector2u window_size = window.getSize ();

                    window_width  = window_size.x;
                    window_height = window_size.y;

                    reset_viewport (window_width, window_height);

                    break;
                }
            }
        }

        glClear (GL_COLOR_BUFFER_BIT);

        update_cube ();
        render_cube ();

        window.display ();
    }
    while (running);

    return (EXIT_SUCCESS);
}
