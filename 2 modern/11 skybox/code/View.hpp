
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2014.03+

#ifndef VIEW_HEADER
#define VIEW_HEADER

    #include "Camera.hpp"
    #include "Skybox.hpp"

    namespace example
    {

        class View
        {
        private:

            Camera camera;
            Skybox skybox;

            int    width;
            int    height;

            float  angle_around_x;
            float  angle_around_y;
            float  angle_delta_x;
            float  angle_delta_y;

            bool   pointer_pressed;
            int    last_pointer_x;
            int    last_pointer_y;

        public:

            View(int width, int height);

            void update   ();
            void render   ();

        public:

            void resize   (int width, int height);
            void on_key   (int key_code);
            void on_drag  (int pointer_x, int pointer_y);
            void on_click (int pointer_x, int pointer_y, bool down);

        };

    }

#endif
