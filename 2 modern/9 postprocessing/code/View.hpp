
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2014.05+

#ifndef VIEW_HEADER
#define VIEW_HEADER

    #include <string>
    #include <memory>
    #include "Cube.hpp"

    namespace example
    {

        class View
        {
        private:

            static const GLsizei framebuffer_width  = 256;
            static const GLsizei framebuffer_height = 256;

            static const std::string     cube_vertex_shader_code;
            static const std::string   cube_fragment_shader_code;
            static const std::string   effect_vertex_shader_code;
            static const std::string effect_fragment_shader_code;

            GLint  model_view_matrix_id;
            GLint  projection_matrix_id;

            GLuint framebuffer_id;
            GLuint depthbuffer_id;
            GLuint out_texture_id;

            Cube   cube;

            GLuint   cube_program_id;
            GLuint effect_program_id;

            GLuint triangle_vbo0;
            GLuint triangle_vbo1;

            float  angle;

            int    window_width;
            int    window_height;

        public:

            View(int width, int height);

            void   update ();
            void   render ();
            void   resize (int  width, int height);

        private:

            void   build_framebuffer      ();
            void   render_framebuffer     ();

            GLuint compile_shaders        (const std::string & vertex_shader_code, const std::string & fragment_shader_code);
            void   show_compilation_error (GLuint  shader_id);
            void   show_linkage_error     (GLuint program_id);

        };

    }

#endif
