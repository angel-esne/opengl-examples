
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.05

#include "Extra.hpp"
#include <Vertex_Shader.hpp>
#include <Fragment_Shader.hpp>
#include <Shader_Program.hpp>

using namespace argb;

namespace example
{

    Extra::Extra(int window_width, int window_height)
    :
        window_width (window_width),
        window_height(window_height),
        vbo_id       (0),
        vao_id       (0),
        program_id   (0)
    {        
        initialized = false;
    }

    Extra::~Extra()
    {
        if (initialized)
        {
            glDeleteVertexArrays (1, &vao_id);
            glDeleteBuffers      (1, &vbo_id);
            glDeleteProgram      (program_id);
        }
    }

    void Extra::prepare_opengl_for_rendering ()
    {
        // Se env�an a la GPU los datos de v�rtices:

        static const GLfloat vertex_attributes[] =
        {
           -0.5f, -0.5f, 0.0f,      // vertex 0 position
            1.0f,  0.0f, 0.0f,      // vertex 0 color
           +0.5f, -0.5f, 0.0f,      // vertex 1 position
            0.0f,  1.0f, 0.0f,      // vertex 1 color
            0.0f,  0.7f, 0.0f,      // vertex 2 position
            0.0f,  0.0f, 1.0f,      // vertex 2 color
        };

        constexpr size_t vertex_size  = sizeof(GLfloat) * 3 * 2;
        constexpr size_t color_offset = sizeof(GLfloat) * 3;

        glGenVertexArrays (1, &vao_id);
        glBindVertexArray (vao_id);

        glGenBuffers (1, &vbo_id);
        glBindBuffer (GL_ARRAY_BUFFER, vbo_id);
        glBufferData (GL_ARRAY_BUFFER, sizeof(vertex_attributes), vertex_attributes, GL_STATIC_DRAW);

        glEnableVertexAttribArray (0);
        glVertexAttribPointer     (0, 3, GL_FLOAT, GL_FALSE, vertex_size, 0);
        glEnableVertexAttribArray (1);
        glVertexAttribPointer     (1, 3, GL_FLOAT, GL_FALSE, vertex_size, (void *)color_offset);

        // Se compilan y se ponen en uso los shaders:

        static const char vertex_shader_code[] =

            "#version 330 core\n"
            ""
            "layout (location = 0) in vec3 vertex_coordinates;"
            "layout (location = 1) in vec3 vertex_color;"
            ""
            "out vec3 front_color;"
            ""
            "void main()"
            "{"
            "   gl_Position = vec4(vertex_coordinates, 1.0);"
            "   front_color = vertex_color;"
            "}";

        static const char fragment_shader_code[] =

            "#version 330 core\n"
            ""
            "in  vec3    front_color;"
            "out vec4 fragment_color;"
            ""
            "void main()"
            "{"
            "    fragment_color = vec4(front_color, 1.0);"
            "}";

        GLuint   vertex_shader_id = glCreateShader (GL_VERTEX_SHADER  );
        GLuint fragment_shader_id = glCreateShader (GL_FRAGMENT_SHADER);

        const char *   vertex_shaders_code[] = {          vertex_shader_code  };
        const char * fragment_shaders_code[] = {        fragment_shader_code  };
        const GLint    vertex_shaders_size[] = { sizeof(  vertex_shader_code) };
        const GLint  fragment_shaders_size[] = { sizeof(fragment_shader_code) };

        glShaderSource  (  vertex_shader_id, 1,   vertex_shaders_code,   vertex_shaders_size);
        glShaderSource  (fragment_shader_id, 1, fragment_shaders_code, fragment_shaders_size);
        glCompileShader (  vertex_shader_id);
        glCompileShader (fragment_shader_id);

        program_id = glCreateProgram ();

        glAttachShader  (program_id,   vertex_shader_id);
        glAttachShader  (program_id, fragment_shader_id);
        glLinkProgram   (program_id);
        glUseProgram    (program_id);

        glDeleteShader  (  vertex_shader_id);
        glDeleteShader  (fragment_shader_id);

        // Se configura el viewport:

        glViewport (0, 0, GLsizei(window_width), GLsizei(window_height));

        initialized = true;
    }

    void Extra::render_using_opengl ()
    {
        glBindVertexArray (vao_id);
        glDrawArrays      (GL_TRIANGLES, 0, 3);
    }

}
