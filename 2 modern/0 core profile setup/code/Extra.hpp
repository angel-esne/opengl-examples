
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.05

#pragma once

#include <glad/glad.h>

namespace example
{

    class Extra
    {
    private:

        int    window_width;
        int    window_height;

        bool   initialized;

        GLuint vbo_id;
        GLuint vao_id;
        GLuint program_id;

    public:

        Extra(int window_width, int window_height);
       ~Extra();

        Extra(const Extra & ) = delete;
        Extra & operator = (const Extra & ) = delete;

        void prepare_opengl_for_rendering ();
        void render_using_opengl ();

    };

}
