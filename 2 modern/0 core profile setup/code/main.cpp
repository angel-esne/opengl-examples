
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2021.05

#include <SFML/Window.hpp>
#include <glad/glad.h>
#include "Extra.hpp"

using namespace sf;

int main ()
{
    constexpr int window_width  = 1024;
    constexpr int window_height = 640;

    Window window
    (
        VideoMode(window_width, window_height),
        "Core Profile Setup",
        sf::Style::Default,
        ContextSettings
        (
            0,                                  // Z-Buffer (en este ejemplo no se usa)
            0,                                  // Stencil Buffer (en este ejemplo no se usa)
            0,                                  // Antialiasing (en este ejemplo no se usa)
            3, 3,                               // Versión de OpenGL: 3.3
            ContextSettings::Core               // Core profile
        )
    );

    // Se intenta initializar GLAD, que buscará todas las funciones de OpenGL 3.3 Core:
    // Si retorna un valor distinto de cero implica que algo falló y se termina el programa.

    if (not gladLoadGL ())
    {
        return -1;
    }

    // Si GLAD se ha podido inicializar, se prepara OpenGL y se crea el bucle principal,
    // dentro del cual se dibujará un triángulo hasta que se cierre la ventana:
    
    example::Extra extra(window_width, window_height);

    extra.prepare_opengl_for_rendering ();

    bool running = true;

    do
    {
        Event event;

        while (window.pollEvent (event))
        {
            if (event.type == Event::Closed)
            {
                running = false;
            }
        }

        // Se borra el framebuffer y se dibuja el triángulo:

        glClear (GL_COLOR_BUFFER_BIT);

        extra.render_using_opengl ();

        // Se actualiza el contenido de la ventana:

        window.display ();
    }
    while (running);

    return EXIT_SUCCESS;
}
